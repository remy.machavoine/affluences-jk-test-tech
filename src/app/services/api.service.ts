import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { AffAvailable } from '../models/aff-available';
import { AffResAvailable } from '../models/aff-res-available';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  /**
   * @description Constructor
   * @param httpClient HttpClient
   */
  constructor(private httpClient: HttpClient) {}

  /**
   * @description Check avaibility
   * @param affAvailable AffAvailable
   * @returns Observable<AffResAvailable>
   */
  public checkAvailability(
    affAvailable: AffAvailable
  ): Observable<AffResAvailable> {
    const path = '/1337/available';

    let url = '';
    if (environment.production) {
      url = `${environment.baseUrl}${environment.pathApi}${path}`;
    }
    url += `${environment.pathApi}${path}`;

    const params = new HttpParams().set('datetime', affAvailable.datetime);

    return this.httpClient.get<AffResAvailable>(url, { params });
  }
}
