import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage-angular';

import { from, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private storage: Storage | null = null;

  /**
   * @description Constructor
   */
  constructor(private ionicStorage: Storage) {}

  /**
   * @description Storage initialization
   */
  public init(): void {
    this.create().subscribe((storage) => (this.storage = storage));
  }

  /**
   * @description Get storage
   * @param key key
   */
  public get(key: string): Observable<any> {
    return from(this.storage?.get(key));
  }

  /**
   * @description Set storage
   * @param key key
   * @param value value
   */
  public set(key: string, value: any): Observable<any> {
    return from(this.storage?.set(key, value));
  }

  /**
   * @description Clear storage
   */
  public clear(): Observable<void> {
    return from(this.storage?.clear());
  }

  /**
   * @description Ready storage
   */
  public ready(): Observable<boolean> {
    return of(this.storage ? true : false);
  }

  /**
   * @description Create storage
   */
  private create(): Observable<Storage> {
    return from(this.ionicStorage.create());
  }
}
