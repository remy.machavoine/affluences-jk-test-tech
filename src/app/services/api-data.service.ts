import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BOOKING_CACHE_KEY } from '../constants/storage.constant';

import { StorageService } from './storage.service';

import { AffAvailable } from '../models/aff-available';
import { AffBooking } from '../models/aff-booking';

@Injectable({
  providedIn: 'root',
})
export class ApiDataService {
  private available$: BehaviorSubject<AffAvailable> = new BehaviorSubject(null);
  public get available(): Observable<AffAvailable> {
    return this.available$.asObservable();
  }

  private booking$: BehaviorSubject<AffBooking> = new BehaviorSubject(null);
  public get booking(): Observable<AffBooking> {
    return this.booking$.asObservable();
  }

  /**
   * @description Constructor
   * @param storageService StorageService
   */
  constructor(private storageService: StorageService) {}

  /**
   * @description Set available
   * @param available AffAvailable
   */
  public setAvailable(available: AffAvailable): void {
    this.available$.next(available);
  }

  /**
   * @description Set booking
   * @param booking AffBooking
   */
  public setBooking(booking: AffBooking): Observable<void> {
    return this.storageService
      .set(BOOKING_CACHE_KEY, booking)
      .pipe(map((_) => this.booking$.next(booking)));
  }
}
