import { Component, OnInit } from '@angular/core';

import { StorageService } from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  /**
   * @description Constructor
   * @param storageService StorageService
   */
  constructor(private storageService: StorageService) {}

  /**
   * @description Initialize the directive or component after Angular first displays the data-bound properties
   * and sets the directive or component's input properties.
   */
  ngOnInit(): void {
    this.storageService.init();
  }
}
