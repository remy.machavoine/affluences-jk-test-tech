export interface AffBooking {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber?: string;
  termsOfUse: boolean;
}
