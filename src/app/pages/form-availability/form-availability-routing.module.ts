import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormAvailabilityPage } from './form-availability.page';

const routes: Routes = [
  {
    path: '',
    component: FormAvailabilityPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormAvailabilityPageRoutingModule {}
