import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormAvailabilityPageRoutingModule } from './form-availability-routing.module';

import { FormAvailabilityPage } from './form-availability.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    FormAvailabilityPageRoutingModule,
  ],
  declarations: [FormAvailabilityPage],
})
export class FormAvailabilityPageModule {}
