import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ToastController } from '@ionic/angular';

import { catchError } from 'rxjs/operators';

import { ApiService } from 'src/app/services/api.service';

import { AffAvailable } from 'src/app/models/aff-available';
import { AffResAvailable } from 'src/app/models/aff-res-available';
import { ApiDataService } from 'src/app/services/api-data.service';

@Component({
  selector: 'app-form-availability',
  templateUrl: './form-availability.page.html',
  styleUrls: ['./form-availability.page.scss'],
})
export class FormAvailabilityPage implements OnInit {
  availabilityForm: FormGroup;

  /**
   * @description Constructor
   * @param formBuilder FormBuilder
   * @param router Router
   * @param apiService ApiService
   * @param apiDataService ApiDataService
   */
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private toastCtrl: ToastController,
    private apiService: ApiService,
    private apiDataService: ApiDataService
  ) {}

  /**
   * @description Initialize the directive or component after Angular first displays the data-bound properties
   * and sets the directive or component's input properties.
   */
  ngOnInit(): void {
    this.availabilityForm = this.formBuilder.group({
      datetime: ['', [Validators.required]],
    });
  }

  /**
   * @description Submit form
   */
  submitForm(): void {
    if (this.availabilityForm.valid) {
      const datetime: string = this.availabilityForm.value.datetime;
      const date: Date = new Date(datetime);
      const affAvailable: AffAvailable = { datetime: date.toISOString() };
      this.apiService
        .checkAvailability(affAvailable)
        .pipe(
          catchError((error) => {
            this.toastCtrl
              .create({
                message: 'Veuillez réesayer ultérieurement.',
                cssClass: `toast toast__error`,
                duration: 2000,
                buttons: [
                  {
                    text: 'OK',
                    role: 'cancel',
                  },
                ],
              })
              .then((errorToast) => errorToast.present());
            return error;
          })
        )
        .subscribe((affResAvailable: AffResAvailable) => {
          this.apiDataService.setAvailable(affAvailable);

          if (affResAvailable.available) {
            this.router.navigate(['booking']);
          } else {
            this.toastCtrl
              .create({
                message: "Le créneau n'est pas disponible.",
                cssClass: `toast toast__info`,
                duration: 2000,
                buttons: [
                  {
                    text: 'OK',
                    role: 'cancel',
                  },
                ],
              })
              .then((infoToast) => infoToast.present());
          }
        });
    }
  }
}
