import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ToastController } from '@ionic/angular';

import { Observable } from 'rxjs';
import { BOOKING_CACHE_KEY } from 'src/app/constants/storage.constant';

import { AffAvailable } from 'src/app/models/aff-available';
import { AffBooking } from 'src/app/models/aff-booking';

import { ApiDataService } from 'src/app/services/api-data.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-form-booking',
  templateUrl: './form-booking.page.html',
  styleUrls: ['./form-booking.page.scss'],
})
export class FormBookingPage implements OnInit {
  bookingForm: FormGroup;

  available$: Observable<AffAvailable>;
  booking$: Observable<AffBooking>;

  /**
   * @description Constructor
   * @param router Router
   * @param formBuilder FormBuilder
   * @param toastCtrl ToastController
   * @param apiDataService ApiDataService
   * @param storageService StorageService
   */
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private apiDataService: ApiDataService,
    private storageService: StorageService
  ) {}

  /**
   * @description Initialize the directive or component after Angular first displays the data-bound properties
   * and sets the directive or component's input properties.
   */
  ngOnInit(): void {
    this.available$ = this.apiDataService.available;
    this.booking$ = this.apiDataService.booking;

    this.booking$.subscribe((booking: AffBooking) => {
      if (booking) {
        this.bookingForm = this.formBuilder.group({
          firstName: [booking.firstName, [Validators.required]],
          lastName: [booking.lastName, [Validators.required]],
          email: [booking.email, [Validators.required, Validators.email]],
          // phoneNumber: [booking.phoneNumber, [Validators.pattern(...)]],
          // TODO: IMPLEMENT PATTERN FOR PHONE NUMBER FORMAT
          phoneNumber: [booking.phoneNumber],
          termsOfUse: [booking.termsOfUse, [Validators.required]],
          save: [false],
        });
      } else {
        this.bookingForm = this.formBuilder.group({
          firstName: ['', [Validators.required]],
          lastName: ['', [Validators.required]],
          email: ['', [Validators.required, Validators.email]],
          // phoneNumber: ['', [Validators.pattern(...)]],
          // TODO: IMPLEMENT PATTERN FOR PHONE NUMBER FORMAT
          phoneNumber: [''],
          termsOfUse: [false, [Validators.required]],
          save: [false],
        });
      }
    });
  }

  /**
   * @description Fired when the component routing to has finished animating.
   */
  ionViewDidEnter(): void {
    this.storageService
      .get(BOOKING_CACHE_KEY)
      .subscribe((booking: AffBooking) => {
        if (booking) {
          this.apiDataService.setBooking(booking).subscribe();
        }
      });

    this.available$.subscribe((available: AffAvailable) => {
      if (!available) {
        this.router.navigate(['availability']);
      }
    });
  }

  /**
   * @description Submit form
   */
  submitForm(): void {
    if (this.bookingForm.valid) {
      if (this.bookingForm.value.termsOfUse) {
        this.toastCtrl
          .create({
            message: 'La réservation à bien été effectuée !',
            cssClass: `toast toast__success`,
            duration: 2000,
            buttons: [
              {
                text: 'OK',
                role: 'cancel',
              },
            ],
          })
          .then((infoToast) => infoToast.present());
      }
      if (this.bookingForm.value.save) {
        const booking: AffBooking = {
          firstName: this.bookingForm.value.firstName,
          lastName: this.bookingForm.value.lastName,
          email: this.bookingForm.value.email,
          phoneNumber: this.bookingForm.value.phoneNumber,
          termsOfUse: this.bookingForm.value.termsOfUse,
        };
        this.apiDataService.setBooking(booking).subscribe();
      }
    }
  }
}
