import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'availability',
    loadChildren: () =>
      import('./pages/form-availability/form-availability.module').then(
        (m) => m.FormAvailabilityPageModule
      ),
  },
  {
    path: 'booking',
    loadChildren: () =>
      import('./pages/form-booking/form-booking.module').then(
        (m) => m.FormBookingPageModule
      ),
  },
  {
    path: '',
    redirectTo: 'availability',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
